import axios from 'axios'

// Fetch Story
export const getStory = async (storyID) => {
    const initialData = await axios.get(`https://hacker-news.firebaseio.com/v0/item/${storyID}.json`)
    const finalData = initialData.data
    return finalData
}

// Get top stories using getStory function
export const getTopStories = async () => {
    const initialData = await axios.get('https://hacker-news.firebaseio.com/v0/topstories.json')
    let storyIDs = initialData.data.slice(0,50)
    const axiosGetStatements = storyIDs.map(async id => await getStory(id))
    const allStoryData = await Promise.all(axiosGetStatements)
    return allStoryData
}

// Fetch Item
export const getItem = async (id) => {
    const initialData = await axios.get(`https://hacker-news.firebaseio.com/v0/item/${id}.json`)
    const finalData = initialData.data
    return finalData
}

// fetch all comments for a particular story
export const getComments = async (storyID) => {
    const storyData = await getStory(storyID)
    const storyCommnentIds = storyData.kids
    const commentsAxiosStatements = storyCommnentIds.map(async (commentID) => { return await getItem(commentID) })
    const commentsData = await Promise.all(commentsAxiosStatements)
    console.log(commentsData)
}

// Get all posts of a user
export const userPosts = async (userName) => {
    const userData = await axios.get(`https://hacker-news.firebaseio.com/v0/user/${userName}.json`);
    const userPostIds = userData.data.submitted.slice(0,30)
    const userPostsAxiosStatements = userPostIds.map(async (postID) => { return await getItem(postID) })
    const allUserPosts = await Promise.all(userPostsAxiosStatements)
    const userStorys = allUserPosts.filter((post)=>post.type=="story")
    return userStorys
}