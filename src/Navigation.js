import React, { Component } from 'react';
import "./Navigation.css"
import { Navbar, Nav, NavDropdown, Container,Form,FormControl,Button } from 'react-bootstrap';

export default class Navigation extends Component {


  constructor(props){
    super(props)

    this.state = ({
      input : ''
    })
  }

  changehandler = (event) => {
    console.log('hello')
}

  render() {

    const { changeHandler, submitHandler } = this.props;

    return (
      <>
        <Navbar bg="dark" variant="dark" className="navigation">
          <Container>
            <Navbar.Brand href="/">Hacker News</Navbar.Brand>
            <Nav className="me-auto">
              <Form className="d-flex" onSubmit={submitHandler}>
                <FormControl
                  type="search"
                  placeholder="Search"
                  className="mr-2"
                  aria-label="Search"
                  onKeyPress={changeHandler}
                />
                <Button variant="outline-success">Search</Button>
              </Form>

            </Nav>
          </Container>
        </Navbar>
      </>
    )
  }
}
