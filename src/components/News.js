import React, { Component } from 'react'
import "../style/News.css"

export default class News extends Component {

    constructor(props){
        super(props)
    }

    render() {
        return (
            <div className="news">
                <div className="title">{this.props.title}</div>
                <div className="by userLinks"><strong>by : </strong><a href={`users/${this.props.by}`}>{this.props.by}</a></div>
                <div className="links"><a href={this.props.url}>Learn More</a></div>
            </div>
        )
    }
}
