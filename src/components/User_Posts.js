import React, { Component } from 'react';
import Post from "./Post";
import { userPosts } from '../apiCalls/apiCalls';
import Navigation from '../Navigation';
import { v4 as uuid } from 'uuid';
import { Spinner } from 'react-bootstrap';
import "../style/User_Posts.css";

export default class User extends Component {

    constructor(props) {
        super(props)

        this.state = ({
            userName: this.props.match.params.name,
            data: [],
            input: '',
            searchedData: [],
            isLoading: true
        })
    }

    async componentDidMount() {
        const finaldata = await userPosts(this.state.userName)
        console.log(finaldata);
        this.setState({ data: finaldata, isLoading: false });
    }

    changeHandler = (event) => {
        this.setState({ input: event.target.value });
        this.filteredData();
    }

    submitHandler = (event) => {
        event.preventDefault();
        this.filteredData();
    }

    filteredData = () => {
        const searchedData = this.state.data.filter((story) => story.title.includes(this.state.input));
        this.setState({ searchedData: searchedData })
    }

    render() {

        const userPosts = this.state.data.map((post) => {
            return <Post
                title={post.title}
                url={post.url}
                text={post.text}
                key={uuid()}
            />
        })

        let filteredData = this.state.searchedData.map((post) => {
            return <Post
                title={post.title}
                url={post.url}
                text={post.text}
                key={uuid()}
            />
        })

        return (
            <>
                <Navigation
                    changeHandler={this.changeHandler}
                    submitHandler={this.submitHandler} />
                <h1>Posts By - {this.state.userName}</h1>

                {this.state.isLoading

                    ? <div className="spinner-container">
                        <Spinner animation="border" variant="light" className="spinnerStyle"/>
                    </div>

                    : (this.state.input === ""

                        ? <>
                            <div className="news-container">
                                {userPosts}
                            </div>
                        </>
                        : <>
                            <h3>Search results according to your input : {this.state.input}</h3>
                            <div className="news-container">
                                {filteredData}
                            </div>
                        </>)
                }

            </>
        )
    }
}