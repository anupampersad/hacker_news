import React, { Component } from 'react'
import "../style/News.css"

export default class User_Post extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="news">
                <div className="title">{this.props.title}</div>
                <div className="post-text" dangerouslySetInnerHTML={{ __html: this.props.text }}/>
                <div className="links"><a href={this.props.url}>Learn More</a></div>
            </div>
        )
    }
}
