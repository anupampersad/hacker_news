import React, { Component } from 'react';
import News from './News';
import "../style/News_List.css";
import { getTopStories, getComments } from '../apiCalls/apiCalls';
import Navigation from '../Navigation';
import { v4 as uuid } from 'uuid';
import { Spinner } from 'react-bootstrap';


export default class News_List extends Component {

    constructor(props) {
        super(props)

        this.state = ({
            data: [],
            input: '',
            searchedData: [],
            isLoading: true
        })
    }


    async componentDidMount() {
        const finaldata = await getTopStories()
        this.setState({ data: finaldata, isLoading: false })
    }

    changeHandler = (event) => {
        this.setState({ input: event.target.value });
        this.filteredData();
    }

    submitHandler = (event) => {
        event.preventDefault();
        this.filteredData();
    }

    filteredData = () => {
        const searchedData = this.state.data.filter((story) => story.title.includes(this.state.input) || story.by.includes(this.state.input));
        this.setState({ searchedData: searchedData })
    }

    render() {

        let newsList = this.state.data.map((news) => {
            return <News
                title={news.title}
                by={news.by}
                url={news.url}
                key={uuid()}
            />
        })

        let filteredData = this.state.searchedData.map((news) => {
            return <News
                title={news.title}
                by={news.by}
                url={news.url}
                key={uuid()}
            />
        })

        return (
            <>
                <Navigation
                    changeHandler={this.changeHandler}
                    submitHandler={this.submitHandler} />

                {

                    this.state.isLoading

                        ? <div className="spinner-container">
                            <Spinner animation="border" variant="light" className="spinnerStyle"/>
                        </div>

                        : (this.state.input === ""

                            ? <>
                                <h1>Top Stories</h1>
                                <div className="news-container">
                                    {newsList}
                                </div>
                            </>
                            : <>
                                <h3>Search results according to your input : {this.state.input}</h3>
                                <div className="news-container">
                                    {filteredData}
                                </div>
                            </>
                        )

                }

            </>
        )
    }
}
