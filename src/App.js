import './App.css';
import News_List from './components/News_List';
import User_Posts from './components/User_Posts';
import { BrowserRouter, Route, Switch } from 'react-router-dom';



import React, { Component } from 'react';

export default class App extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (

      <div className="main">

        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={News_List} />
            <Route path="/users/:name" component={User_Posts} />
          </Switch>
        </BrowserRouter>

      </div>

    )
  }
}